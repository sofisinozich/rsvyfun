# Significance Testing ----------------------------------------------------
### pdiff and moe are directly taken from the spreadsheet
pdiff<-function(p1,n1,p2,n2,deff1=1,deff2=deff1){
  k1<-deff1*p1*(1-p1)/n1
  k2<-deff2*p2*(1-p2)/n2
  k_sig<-sqrt(k1+k2)*qnorm(.025,lower.tail=FALSE)
  k_marg<-sqrt(k1+k2)*qnorm(.05,lower.tail=FALSE)
  diff<-abs(p1-p2)
  siglist<-ifelse((diff-k_sig)>=0,siglist<-"*",ifelse((diff-k_marg)>=0,siglist<-".",siglist<-""))
  difftable<-data.frame(cbind(p1,p2),Diff=diff,"Diff@0.95"=k_sig,"Diff@0.9"=k_marg,"Sig"=siglist)
  return(difftable)
}

moe<-function(n,deff=1,p1=0.5,p2=NULL){
  k_sig<-sqrt(deff*p1*(1-p1)/n)*qnorm(.025,lower.tail=FALSE)
  k_marg<-sqrt(deff*p1*(1-p1)/n)*qnorm(.05,lower.tail=FALSE)
  
  if(!is.null(p2)) {
    diff<-abs(p1-p2)
    siglist<-ifelse((diff-k_sig*2)>=0,siglist<-"*",ifelse((diff-k_marg*2)>=0,siglist<-".",siglist<-""))
    difftable<-data.frame(cbind(p1,p2),Diff=diff,"MOE@0.95"=k_sig,"MOE@0.9"=k_marg,"Sig"=siglist)
  }
  else {
    difftable<-data.frame(N=n,p1,"MOE@0.95"=k_sig,"MOE@0.9"=k_marg)
  }
  
  return(difftable)
  
}