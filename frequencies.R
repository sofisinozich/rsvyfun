library(questionr) #for crosstabs v0.6.0
library(memisc)
# Frequencies -------------------------------------------------------------

wfreq<-function(x,w=NULL,d=NULL,q=0) {
  if (is.null(d)){ 
    if(!is.null(w)) {
      wfreqtable<-wtd.table(x,weights=w)
      freq_net_helper(wfreqtable,q)
    }
    else {
      freqtable<-table(x)
      freq_net_helper(freqtable,q)
    }
    
  }
  
  else { 
    x<-deparse(substitute(x))
    w<-deparse(substitute(w))
    newdata<-freq_data(x,w,d)
    if (dim(newdata)[2]>1) {
      wfreq(newdata[,1],newdata[,2],q=q) 
    }
    else {
      wfreq(newdata[,1],q=q)
    }
    
  }
}

#eventually may merge this with xtabs cros_data for one unified function
freq_data<-function(x,w,d){
  if(is.data.frame(d) | is.data.set(d)) {
    d<-as.data.frame(d)
    if (all(c(x,w) %in% names(d))) {
      return(data.frame(d[,x],d[,w]))
    }
    else if (x %in% names(d)) {
      return(data.frame(d[,x]))
    }
    else {
      stop("One of these variables is not in the dataset.")
    }
  }
  else { #things that aren't dataframes or datasets
    stop("Not a supported data type. Please convert to a dataframe.")
  }
  
}

#Helper function to actually do the nets
freq_net_helper<-function(freqtable,q) {
  if (all(q>0)) { 
    q<-sort(q)
    
    netted<-sum(freqtable[q])
    netnames<-paste0(substring(names(freqtable)[q],1,10),collapse="/")
    freqtable[q[1]]<-netted
    names(freqtable)[q[1]]<-paste0(netnames," NET")
    freqtable<-freqtable[-(q[-1])]
  }
  perctable<-freq(freqtable)[,-c(1,3),drop=FALSE] #Remove the val% cause it should be the same and the weighted n
  freqtable<-round(perctable,0)
  return(freqtable)
}
