# README #

`rsvyfun` includes convenience functions for common survey-related tasks in R, such as weighted frequencies, crosstabs, regressions, and significance testing. It is intended to help you replace SPSS as a daily driver for basic tasks.

### How to use ###

* Download and source the files that have the functions that interest you.
* Eventually there may be a package to streamline this.

### What is here ###

* `crosstabs.r` contains functions to do weighted and unweighted row and column crosstabs, including netting.
* `frequencies.r` contains the weighted frequency function, including netting.
* `testing.r` contains convenience functions for significance testing of percentages and calculating margin of error.
* `modeling.r` contains functions to do "pretty" output of regressions that include standardized betas.

### Who did this ###

`rsvyfun` is entirely the fault of Sofi Sinozich.

### Why "fun" ###

* R is fun, surveys are fun, functions are fun. Hence the name. (Plus `survey` is already taken.)